package com.mularo.codingtask.presentation

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.mularo.codingtask.domain.images.GetImagesUseCase

class ViewModelFactory(private val getImagesUseCase: GetImagesUseCase) :
    ViewModelProvider.Factory {

    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return modelClass.getConstructor(GetImagesUseCase::class.java)
            .newInstance(getImagesUseCase)
    }
}