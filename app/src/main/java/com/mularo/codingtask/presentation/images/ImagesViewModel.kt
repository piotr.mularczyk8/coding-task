package com.mularo.codingtask.presentation.images

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.mularo.codingtask.domain.images.GetImagesUseCase
import com.mularo.codingtask.domain.images.entities.Images
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.onCompletion
import kotlinx.coroutines.flow.stateIn
import timber.log.Timber

class ImagesViewModel(getImagesUseCase: GetImagesUseCase) : ViewModel() {
    val imagesFlow: StateFlow<ImagesUIState> = getImagesUseCase
        .invoke()
        .map { images ->
            images.sortByPublished()
            ImagesUIState.Success(images) as ImagesUIState
        }
        .onCompletion {
            Timber.i("Images Downloaded")
        }.catch {
            emit(ImagesUIState.Error("something went wrong"))
        }
        .stateIn(
            scope = viewModelScope,
            initialValue = ImagesUIState.Loading,
            started = SharingStarted.WhileSubscribed(stopTimeoutMillis = 10000)
        )
}

private fun Images.sortByPublished() {
    items = items.sortedBy { it.published }
}
