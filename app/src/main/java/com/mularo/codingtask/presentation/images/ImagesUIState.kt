package com.mularo.codingtask.presentation.images

import com.mularo.codingtask.domain.images.entities.Images

sealed class ImagesUIState {
    object Loading : ImagesUIState()
    class Success(val images: Images) : ImagesUIState()
    class Error(val message: String) : ImagesUIState()
}