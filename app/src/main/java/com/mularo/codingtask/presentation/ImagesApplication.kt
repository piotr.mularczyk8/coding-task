package com.mularo.codingtask.presentation

import android.app.Application
import androidx.room.Room
import com.mularo.codingtask.domain.images.room.ImagesDatabase
import timber.log.Timber

class ImagesApplication : Application() {
    companion object {
        lateinit var database: ImagesDatabase
    }

    override fun onCreate() {
        super.onCreate()
        initRoomDatabase()
        initTimber()
    }

    private fun initTimber() {
        Timber.plant(Timber.DebugTree())
    }

    private fun initRoomDatabase() {
        database = Room.databaseBuilder(
            applicationContext,
            ImagesDatabase::class.java,
            "images_database"
        ).build()
    }
}