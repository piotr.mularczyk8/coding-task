package com.mularo.codingtask.presentation.utils

import com.mularo.codingtask.data.images.dto.Domain
import com.mularo.codingtask.data.images.dto.Dto

fun List<Dto>.asDomain(): List<Domain> {
    return this.map { it.asDomain() }
}