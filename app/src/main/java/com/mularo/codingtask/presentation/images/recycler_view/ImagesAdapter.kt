package com.mularo.codingtask.presentation.images.recycler_view

import android.content.Intent
import android.net.Uri
import android.text.Html
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.mularo.codingtask.R
import com.mularo.codingtask.databinding.RecyclerViewItemBinding
import com.mularo.codingtask.domain.images.entities.Images
import com.mularo.codingtask.domain.images.entities.Item


class ImagesAdapter(private val images: List<Item>) : RecyclerView.Adapter<ImagesAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val item =
            RecyclerViewItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)

        return ViewHolder(item)
    }

    override fun getItemCount(): Int {
        return images.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val image = images[position]
        holder.binding.tvDescription.text =
            Html.fromHtml(image.description, Html.FROM_HTML_MODE_COMPACT)

        val rootView = holder.binding.root
        loadImage(rootView, image, holder)
        holder.binding.cvItem.setOnClickListener {
            openLinkInBrowser(image, rootView)
        }
    }

    private fun openLinkInBrowser(
        image: Item,
        rootView: View
    ) {
        val intent = Intent(Intent.ACTION_VIEW)
        intent.data = Uri.parse(image.link)
        rootView.context.startActivity(intent)
    }

    private fun loadImage(
        rootView: View,
        image: Item,
        holder: ViewHolder
    ) {
        Glide.with(rootView)
            .load(image.media.m)
            .apply(
                RequestOptions()
                    .placeholder(R.drawable.ic_image_placeholder)
                    .error(R.drawable.ic_image_error)
            )
            .into(holder.binding.ivImage)
    }

    class ViewHolder(val binding: RecyclerViewItemBinding) : RecyclerView.ViewHolder(binding.root)
}