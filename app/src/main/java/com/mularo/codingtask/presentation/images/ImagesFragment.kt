package com.mularo.codingtask.presentation.images

import android.content.res.Configuration
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import androidx.recyclerview.widget.GridLayoutManager
import com.mularo.codingtask.R
import com.mularo.codingtask.data.images.repository_impl.ImagesRepositoryImpl
import com.mularo.codingtask.databinding.FragmentImagesBinding
import com.mularo.codingtask.domain.images.GetImagesUseCase
import com.mularo.codingtask.presentation.ViewModelFactory
import com.mularo.codingtask.presentation.images.recycler_view.ImagesAdapter
import com.mularo.codingtask.presentation.utils.toast
import kotlinx.coroutines.launch

class ImagesFragment : Fragment() {

    private val viewModel: ImagesViewModel by viewModels {
        ViewModelFactory(GetImagesUseCase(ImagesRepositoryImpl()))
    }
    private var _binding: FragmentImagesBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentImagesBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        initObservers()
        initRecyclerView()
        super.onViewCreated(view, savedInstanceState)
    }

    private fun initRecyclerView() {
        val gridLayoutManager = GridLayoutManager(requireContext(), resources.getInteger(R.integer.grid_column_count))
        binding.rvImages.layoutManager = gridLayoutManager
    }

    private fun initObservers() {
        lifecycleScope.launch {
            repeatOnLifecycle(Lifecycle.State.STARTED) {
                viewModel.imagesFlow.collect {
                    render(it)
                }
            }
        }
    }

    private fun render(uiState: ImagesUIState) {
        when (uiState) {
            ImagesUIState.Loading -> binding.cpiLoading.show()
            is ImagesUIState.Success -> {
                binding.rvImages.adapter = ImagesAdapter(uiState.images.items)
                binding.cpiLoading.hide()
            }

            is ImagesUIState.Error -> {
                context.toast(uiState.message)
                binding.cpiLoading.hide()
            }
        }
    }
}