package com.mularo.codingtask.domain.images

import com.mularo.codingtask.domain.images.entities.Images
import kotlinx.coroutines.flow.Flow

class GetImagesUseCase(private val imagesRepository: ImagesRepository) {

    fun invoke(): Flow<Images> {
        return imagesRepository.getImages()
    }
}