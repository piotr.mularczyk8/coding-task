package com.mularo.codingtask.domain.images

import com.mularo.codingtask.domain.images.entities.Images
import kotlinx.coroutines.flow.Flow

interface ImagesRepository {
    fun getImages(): Flow<Images>
}