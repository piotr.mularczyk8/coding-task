package com.mularo.codingtask.domain.images.room

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.mularo.codingtask.domain.images.entities.Images

@Dao
interface ImagesDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertImages(images: Images)

    @Query("SELECT * FROM images")
    suspend fun getAllImages(): Images?
}