package com.mularo.codingtask.domain.images.entities

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.mularo.codingtask.data.images.dto.Domain

@Entity(tableName = "images")
data class Images(
    @PrimaryKey val id: Int = 0,
    val description: String,
    val generator: String,
    var items: List<Item>,
    val link: String,
    val modified: String,
    val title: String
) : Domain

data class Item(
    val author: String,
    val author_id: String,
    val date_taken: String,
    val description: String,
    val link: String,
    val media: Media,
    val published: String,
    val tags: String,
    val title: String
) : Domain

data class Media(
    val m: String
) : Domain