package com.mularo.codingtask.domain.images.room

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.mularo.codingtask.domain.images.entities.Images

@Database(entities = [Images::class], version = 1)
@TypeConverters(Converters::class)
abstract class ImagesDatabase : RoomDatabase() {
    abstract fun imagesDao(): ImagesDao
}