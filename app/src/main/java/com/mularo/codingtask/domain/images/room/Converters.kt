package com.mularo.codingtask.domain.images.room

import androidx.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.mularo.codingtask.domain.images.entities.Item

class Converters {
    @TypeConverter
    fun fromItemList(itemList: List<Item>): String {
        val gson = Gson()
        return gson.toJson(itemList)
    }

    @TypeConverter
    fun toItemList(itemListString: String): List<Item> {
        val gson = Gson()
        val type = object : TypeToken<List<Item>>() {}.type
        return gson.fromJson(itemListString, type)
    }
}

