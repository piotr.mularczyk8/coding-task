package com.mularo.codingtask.data.images.dto

interface Dto {
    fun asDomain(): Domain
}

interface Domain