package com.mularo.codingtask.data.images.dto

import com.mularo.codingtask.domain.images.entities.Images
import com.mularo.codingtask.domain.images.entities.Item
import com.mularo.codingtask.domain.images.entities.Media
import com.mularo.codingtask.presentation.utils.asDomain

@Suppress("UNCHECKED_CAST")
data class ImagesDto(
    val description: String,
    val generator: String,
    val items: List<ItemDTO>,
    val link: String,
    val modified: String,
    val title: String
) : Dto {
    override fun asDomain(): Domain {
        return Images(
            description = description,
            generator = generator,
            items = items.asDomain() as List<Item>,
            link = link,
            modified = modified,
            title = title
        )
    }
}



data class ItemDTO(
    val author: String,
    val author_id: String,
    val date_taken: String,
    val description: String,
    val link: String,
    val media: MediaDTO,
    val published: String,
    val tags: String,
    val title: String
) : Dto {
    override fun asDomain(): Domain {
        return Item(
            author = author,
            author_id = author_id,
            date_taken = date_taken,
            description = description,
            link = link,
            media = media.asDomain() as Media,
            published = published,
            tags = tags,
            title = title
        )
    }
}

data class MediaDTO(
    val m: String
) : Dto {
    override fun asDomain(): Domain {
        return Media(m = this.m)
    }
}