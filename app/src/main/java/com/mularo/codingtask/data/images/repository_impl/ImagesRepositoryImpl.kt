package com.mularo.codingtask.data.images.repository_impl

import com.mularo.codingtask.data.api.FlickrApi
import com.mularo.codingtask.data.api.RetrofitHelper
import com.mularo.codingtask.domain.images.ImagesRepository
import com.mularo.codingtask.domain.images.entities.Images
import com.mularo.codingtask.presentation.ImagesApplication
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow


class ImagesRepositoryImpl : ImagesRepository {
    private var flickrApi: FlickrApi = RetrofitHelper.getInstance().create(FlickrApi::class.java)

    override fun getImages(): Flow<Images> = flow {
        val imagesFromRoom = ImagesApplication.database.imagesDao().getAllImages()
        if (imagesFromRoom != null) {
            emit(imagesFromRoom)
        }
        val images = flickrApi.getImages().asDomain() as Images
        ImagesApplication.database.imagesDao().insertImages(images)
        emit(images)
    }
}
