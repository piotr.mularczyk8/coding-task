package com.mularo.codingtask.data.api

import com.mularo.codingtask.data.images.dto.ImagesDto
import retrofit2.http.GET

interface FlickrApi {
    @GET("/services/feeds/photos_public.gne?format=json&tags=cat&nojsoncallback=1")
    suspend fun getImages(): ImagesDto
}